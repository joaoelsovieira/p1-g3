var moment = require("../libs/moment.js");

var server = "localhost";
var port = 5000;
var serverUrl = server + ":" + port;

var clients = [];
var nicknames = [];
var timeoutsec = 90 * 1000;
var channels = {};

var commands = [
    "NICK", "USER", "QUIT",
    "JOIN", "PART", "TOPIC", "NAMES", "LIST",
    "PRIVMSG",
    "MOTD",
    "WHO",
    "PING", "PONG"
];

/**
 * Escreve na tela o log de um comando executado para o cliente
 * @param socket
 * @param code
 * @param message
 * @param param1
 */
function log(socket, code, message, client) {

    var strings = [];

    if (code) {
        strings = [':' + serverUrl, code, socket.nickname || '*', message];

    } else {
        strings = [':' + socket.nickname + '!' + socket.nickname + '@' + server, message]
    }

    var selectedClient = client || socket;
    selectedClient.write(strings.join(" ") + "\n");

}

var functions = {

	/**
	 * Altera o nick de um usuário
	 * @param socket
	 * @param args
	 */
    NICK: function (socket, args) {

        var nickname = args[0];

        if (nickname) {

            // Checar se é o nickname válido
            if (nickname.match(/^[0-9a-zA-Z]+$/)) {

                // Nick disponível
                if (nicknames.indexOf(nickname) == -1) {

                    // Remover nickname do socket, caso exista
                    if (socket.nickname && nicknames.indexOf(socket.nickname) >= 0) {
                        nicknames.splice(nicknames.indexOf(socket.nickname), 1);
                    }
    
                    socket.nickname = nickname;
                    nicknames.push(nickname);

                } else {
                    log(socket, 433, nickname + " :Nickname is already in use");
                }

            } else {
                log(socket, 432, nickname + " :Erroneous nickname");
            }

        } else {
            log(socket, 431, ":No nickname given");
        }
    },

	/**
	 * Especifica username, hostname e realname de um novo usuário
	 * @param socket
	 * @param args
	 */
    USER: function (socket, args) {

        var username = args[0];
        var mode = args[1];
        var unused = args[2];
        var realname = args[3];

        if (socket.username) {
            log(socket, 462, ':Unauthorized command (already registered)');

        } else {

            if (username && mode && realname) {
                socket.username = username;
                socket.mode = mode;
                socket.realname = realname;

            } else {
                log(socket, 462, 'USER :Not enough parameters');
            }
        }
    },

    /**
     * Encerra conexão do cliente
     * @param socket
     * @param args
     */
    QUIT: function (socket, args) {

        // Liberar nickname
        nicknames.splice(nicknames.indexOf(socket.nickname), 1);
        
        if (args) {
            socket.quit_message = args[0];
        }

        socket.end();

    },

    /**
     * Insere um cliente em um ou mais canais
     * @param socket
     * @param args
     */
    JOIN: function (socket, args) {

        var argsChannels = args[0] ? args[0].split(",") : [];
        var argsKeys = args[1] ? args[1].split(",") : [];

        if (argsChannels.length > 0) {

            // Sair de todos os canais
            if (argsChannels.length == 1 && argsChannels[0] == 0) {

                var oldChannels = JSON.parse(JSON.stringify(socket.channels));
                oldChannels.forEach(function(channel) {
                    functions.leaveChannel(socket, channel);
                });

            } else {

                var validChannels = [];
                var keys = [];

                argsChannels.forEach(function(channel, index) {
                    
                    if (channel !== 0 && channel) {
                        validChannels.push(channel);
                        keys.push(argsKeys[index] || false);
                    }

                });

                validChannels.forEach(function(channel, index) {

                    if (channel.charAt(0) == '#') {

                         // Criar canal, caso não exista
                        if (channels[channel] === undefined) {
                            channels[channel] = { name: channel, key: keys[index], clients: [] };
                        }

                        // Verificar se o usuário já não pertence ao canal
                        if (socket.channels.indexOf(channel) == -1) {

                            // Independente da key, adiciona o cliente no canal (DÚVIDA)
                            socket.channels.push(channel);
                            channels[channel].clients.push(socket);

                            // Mensagem de sucesso
                            log(socket, false, 'JOIN :' + channel);

                            // Toda vez que entrar em um canal, mostrar os usuários conectados a ele
                            functions.listChannelNames(socket, channel);
                        
                        // (DÚVIDA)
                        } else {

                        }

                    } else {
                        log(socket, 479, channel + ' :Illegal channel name');
                    }

                });
            }

        } else {
            log(socket, 461, ":Not enough parameters");
        }

    },

    /**
     * Comando executado para sair de um canal
     * @param socket
     * @param args
     */
    PART: function (socket, args) {

        if (args[0]) {

            var argsChannels = args[0].split(",");

            argsChannels.forEach(function(channel, index) {
                functions.leaveChannel(socket, channel, args[1]);
            });

        } else {
            log(socket, 461, ':Not enough parameters');
        }
    },

    /**
     * Remover um cliente de um canal
     * @param socket
     * @param channel
     * @param message
     */
    leaveChannel: function(socket, channel, message) {

        if (channels[channel]) {

            let channelIndexOnSocket = socket.channels.indexOf(channel);
            let socketIndexOnChannel;

            if (channelIndexOnSocket >= 0) {

                // Remover canal do cliente
                socket.channels.splice(channelIndexOnSocket, 1);

                // Remover cliente do canal
                channels[channel].clients.forEach(function(client, index) {
                    if (client.name == socket.name) socketIndexOnChannel = index; return false;
                });

                if (socketIndexOnChannel >= 0) {

                    channels[channel].clients.splice(socketIndexOnChannel, 1);

                    if (channels[channel].clients.length == 0) {
                        delete channels[channel];
                    }

                    // Sucesso: saiu do canal
                    log(socket, false, 'PART ' + channel);

                }

            } else {
                log(socket, 442, channel + ":You're not on that channel");
            }

        } else {
            log(socket, 403, channel + ' :No such channel');
        }
 
    },

    TOPIC: function (socket, args) {
        console.log("topic function", args);
    },

    /**
     * Visualizar todos os usuários em um ou mais canais
     * @param socket
     * @param args
     */
    NAMES: function (socket, args) {

        var stringChannels = args[0];
        var arrChannels = args[0].split(",");

        if (stringChannels && arrChannels.length > 0) {

            arrChannels.forEach(function(channel) {
                functions.listChannelNames(socket, channel);
            });
        
        } else {
            log(socket, 366, "* :End of /NAMES list.");
        }

    },

    /**
     * Visualizar todos os usuários em um canal
     * @param socket
     * @param channel
     */
    listChannelNames: function(socket, channel) {

        var selectedChannel = channels[channel];
        if (selectedChannel && selectedChannel.clients.length > 0) {

            var nicknames = [];
            selectedChannel.clients.forEach(function(client, index) {
                nicknames.push(index == 0 ? ('@' + client.nickname) : client.nickname);
            });

            var message = "= " + channel + " :" + nicknames.reverse().join(" ");
            log(socket, 353, message);
            log(socket, 366, channel + ' :End of /NAMES list.');

        } else {
            log(socket, 366, channel + ' :End of /NAMES list.');
        }

    },

    LIST: function (socket, args) {
        console.log("list function", args);
    },

    PRIVMSG: function (socket, args) {

        var target = args[0];
        var message = args[1];

        if (target) {

            if (message) {

                // Mensagem para server
                if (target.charAt(0) == "$") {

                // Mensagem para host
                } else if (target.charAt(0) == "#") {

                // Mensagem para nickname
                } else {

                    var search = { nickname: "", username: "", host: "", server: "" };
                    var active = "nickname";

                    for (var i=0; i<target.length; i++) {

                        var char = target.charAt(i);

                        if (char == "!") {
                            active = "username";

                        } else if (char == "@") {
                            active = "server";

                        } else if (char == "%") {
                            active = "host";

                        } else {
                            search[active] = search[active].concat(char);
                        }
                    }

                    clients.forEach(function(client) {

                        var nicknameChecked = client.nickname == search.nickname;
                        var usernameChecked = !search.username || client.username == search.username;
                        var hostChecked = !search.host || client.host == search.host || true;
                        var serverChecked = !search.server || client.server == search.server || true;

                        if (nicknameChecked && usernameChecked && hostChecked && serverChecked) {
                            log(socket, false, 'PRIVMSG ' + client.nickname + ' :' + message, client);
                        }

                    });

                }

            } else {
                log(socket, 412, ':No text to send');
            }

        } else {
            log(socket, 411, ':No recipient given (PRIVMSG)');
        }

    },

    MOTD: function (socket, args) {
        console.log("motd function", args);
    },

    /**
     * Realiza uma busca na lista de clientes do servidor
     * @param socket
     * @param args
     */
    WHO: function (socket, args) {
        
        var mask = args[0];
        var checkOperator = args[1] == "o";

        if (mask) {

            clients.forEach(function(client) {

                var usernameCheck = client.username && client.username.indexOf(mask) >= 0;
                var nicknameCheck = client.nickname && client.nickname.indexOf(mask) >= 0;
                var realnameCheck = client.realname && client.realname.indexOf(mask) >= 0;

                if (usernameCheck || nicknameCheck || realnameCheck) {
                    socket.write("Cliente encontrado: " + client.nickname + "\n");
                }

            });

        } else {
            socket.write("ERRO: Máscara não específicada.\n");
        }   
    },

    /**
     * Realiza um ping no servidor local
     * @param socket
     * @param args
     */
    PING: function (socket, args) {
        
        var token = args[0];
        
        if (token) {
            functions.PONG(socket, "localhost:"+port, token);
            
        } else {
            socket.write("ERRO: Token não especificado.\n");
        }
    },

    /**
     * Utilizado como forma de confirmação de um ping realizado anteriormente
     */
    PONG: function (socket, who, token) {
        
        // Sempre que for chamada, essa função irá definir o cliente como ativo
        // e resetar seu timeout
        socket.active = true;
        socket.setTimeout(timeoutsec);
        
        // Se a mensagem vier com todos os parâmetros, significa que veio do servidor
        // para ser enviada para um cliente confirmando o PING que ele havia executado
        if (who && token) {
            //socket.write("PONG "+who+": "+token+"\n");
        }
    },
}

/**
 * Retorna os parâmetros em uma string
 * @param string 
 */
var getArgs = function (string) {

    var parts = string.split(" ");
    var args = [];

    var tmp = [];
    var singleWord = true;

    parts.forEach(function (part, index) {

        if (part.charAt(0) == ":") {

            part = part.slice(1);
            singleWord = false;

            if (tmp.length > 0) {
                args.push(tmp.join(" "));
            }

            tmp = [];
        }

        if (singleWord) {
            args.push(part);

        } else {

            tmp.push(part);

            if (index == parts.length - 1) {
                args.push(tmp.join(" "));
            }
        }

    });

    return args;
}

/**
 * Analisa a mensagem digitada pelo usuário e executa o comando específicado
 * @param socket
 * @param buffer 
 */
var execute = function (socket, buffer) {

    var string = String(buffer).trim();
    if (string.length > 0) {

        var args = getArgs(string);
        var command = args[0];
    
        if (socket.nickname || socket.username || command == "USER" || command == "NICK") {
    
            // Comando inválido
            if (commands.indexOf(command) == -1) {
                log(socket, 421, command + ' :Unknown command');
        
            // Executa a função selecionada
            } else {
                args.shift();
                functions[command](socket, args);
            }
    
        // Erro de ciclo
        } else {
            log(socket, 451, ':You have not registered');
        }
    }

    if (command !== "PONG") functions.PONG(socket);
}

/**
 * Checar inatividade de conexão de algum cliente
 * @param socket 
 */
var checkConnection = function(socket) {
    
    if (socket.active) {
        functions.PING(socket, "localhost:"+port);
        socket.active = false;
        socket.setTimeout(timeoutsec);
        
    // Finalizar conexão por inatividade
    } else {
        socket.write("ATENÇÃO: Desconectado por inatividade.\n");
        functions.QUIT(socket);
    }
}

net = require('net');
net.createServer(function (socket) {

    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    socket.active = false;
    socket.channels = [];
    socket.setTimeout(timeoutsec);

    // Inserir cliente na lista
    clients.push(socket);

    // Função executada sempre que o cliente realiza uma requisição
    socket.on('data', function (buffer) {
        execute(socket, buffer);
    });

    // Função executada sempre que um cliente atinge o timeout
    socket.on("timeout", function() {
        checkConnection(socket);
    });

    // Função executada sempre que um cliente encerra a conexão
    socket.on('end', function (message) {
        
        clients.splice(clients.indexOf(socket), 1);

        // Remover cliente dos canais
        var clientChannels = JSON.parse(JSON.stringify(socket.channels));
        clientChannels.forEach(function(channel) {
            functions.leaveChannel(socket, channel);
        });

    });

    /* var users = [
        { nickname: "victor", realname: "Victor Nolêto" },
        { nickname: "lucio", realname: "Lúcio" },
        { nickname: "caua", realname: "Cauã" },
        { nickname: "vitor", realname: "Vitor Oliveira" },
    ];

    var selectedUser = users[clients.length - 1];
    console.log(selectedUser);

    if (selectedUser) {
        functions.NICK(socket, [ selectedUser.nickname ]);
        functions.USER(socket, [ selectedUser.nickname, "1", "*", selectedUser.realname ]);
    } */

}).listen(port);